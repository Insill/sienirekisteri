/**
 * @(#)Sienilista.java
 *
 * <p>Luokka, joka toteuttaa sienilistan. Luokassa hoidetaan sienien lisäys ja poisto listasta, haku sekä tulostus</p>
 *
 * <p>Luokassa toteutetaan Serializable-rajapinta, jotta tietoja voidaan käsitellä binäärimuotoisena.<p>
 * @author Insill
 * @version 1.00 2012/1/30
 */

import java.util.Calendar;
import java.io.Serializable;

public class Sienilista implements Serializable {
 	/** Muuttuja joka sisältää listan ensimmäisen alkion */
 	private Sieni ekaListalla;

   /** Oletuskonstruktiori joka luo oletusarvoisen Sienilistan. */
    public Sienilista() {
    /** Listan ensimmäinen alkio on null eli tyhjä. */
    	ekaListalla = null;
    }

	/**
	 * Sienen lisäysmetodi.
	 * @param uusiSieni Käyttäjän lisäämä sieni.
	 *
	 * @return Tyhjä viite, jos uusiSieni on null, muulloin tyhjä merkkijono.
	 */
    public String lisaaSieni(Sieni uusiSieni) {
		if(uusiSieni == null)
			return "Tyhjä viite!";	//

		uusiSieni.setSeuraava(ekaListalla);
		ekaListalla = uusiSieni;
		System.out.println("\n");
		System.out.println("Sieni on lisätty! \n");
		System.out.println(uusiSieni.toString());
		return "";

	}
/**
 *Sienten haku myrkyllisyyden mukaan.
 *
 *@param myrkyllisyys Sienen myrkyllisyys kokonaislukuna (väliltä 1-3)
 *@return Haussa löydetyt sienet, jotka vastaavat haettua myrkyllisyysastetta.
 */

	public String haeSienetMyrkky (int myrkyllisyys) {
		Sieni tulostus1 = ekaListalla;
		String sSienet1 = " ";
		while(tulostus1 != null) {
			if(myrkyllisyys == tulostus1.getMyrkyllisyys()) // Jos myrkyllisyys vastaa getMyrkyllisyys-metodissa saatua myrkyllisyyttä...
				sSienet1 = tulostus1 + "\n" + sSienet1;
			tulostus1 = tulostus1.getSeuraava(); // Sienet haetaan ja tulostetaan
		}
		if(sSienet1.equals(" "))
			System.out.println("Sieniä ei löytynyt valitulla myrkyllisyysasteella.");
			return sSienet1;
	}
/**
 * Sienten haku lajin mukaan.
 * Jos laji löytyy haussa, lisätään sSienet-muuttujaan tulostuksessa löytyvä sieni ja haetaan sitten seuraava tulostettava.
 * Jos sSienet-muuttuja jää tyhjäksi, tulostetaan ilmoitus siitä ettei sieniä löytynyt.
 *@param laji Sienen laji, merkkijono
 *@return Haussa löydetyt sienet
 *
 */
	public String haeSienetLaji (String laji) { // Sienien haku lajin perusteella
		Sieni tulostus = ekaListalla; // tulostus aloitetaan listan ensimmäisestä
		/** Luodaan String, johon kerätään sieniä */
		String sSienet = " ";
		while(tulostus != null) { // Kun tulostettavaa on...
		
			if(laji.equals(tulostus.getLaji()))
			sSienet = tulostus + "\n" + sSienet; // sSieneen lisätään sieniä
			tulostus = tulostus.getSeuraava(); // haetaan seuraava tulostettava

		}
		if(sSienet.equals(" "))
				System.out.println("Sieniä ei löytynyt");

		return sSienet;
	}
/**
 * <p>Sienten poistometodi. Käyttäjältä kysytään sienen lajia ja päivämäärää. Tämän jälkeen vuotta, kuukautta ja vuotta vertaillaan get-metodeilla saataviin tietoihin ja vastaavasti lajia
 * get-metodeilla saataviin tietoihin.<p>
 * <p>Jos tiedot vastaavat (kun listalla on sieniä, ekaListalla-muuttuja ei ole tyhjä), poistetaan alkio ja sitten määritellään seuraava alkio listan ensimmäiseksi. </p>
 *
 *@param laji Sienen laji
 *@param paiva keräyspäivä kokonaislukuna
 *@param kuukausi kuukausi kokonaislukuna
 *@param vuosi vuosi kokonaislukuna
 *
 *@return viesti joslista on tyhjä tai jos poistettavaa sientä ei löydy
 *
 */

  	public String poistaSieni (String laji, int paiva, int kuukausi, int vuosi) {
	/** Boolean-muotoinen muuttuja <code> poistettu </code> merkitsemään, onko sieni poistettu, oletuksena arvolla <b>false</b> */
		boolean poistettu = false;
	/** Calendar-muotoinen muuttuja, johon "kootaan" parametreinä saatavat tiedot */
		Calendar paivamaara;

		if((ekaListalla == null)) {	// Onko tyhjä?
		 

			return "Poisto ei onnistu, tyhjä lista"; // Jos on, tulostetaan viesti
		}


		while(ekaListalla != null) {

			paivamaara = ekaListalla.getKeraysPaiva();
			if ((paivamaara.get(Calendar.YEAR)== vuosi && paivamaara.get(Calendar.MONTH)==(kuukausi-1) && paivamaara.get(Calendar.DAY_OF_MONTH) ==paiva)
			   && laji.equals(ekaListalla.getLaji())) { // Jos päivämäärä täsmää ja laji vastaa getLajilla saatavaa, tehdään poisto
			
				System.out.println("Poistetaan\n"+ekaListalla);
				ekaListalla = ekaListalla.getSeuraava();
				poistettu = true; // sieni on poistettu, asetetaan arvoksi true

			}
			else // muutoin keskeytys
				 break;
			}
		// Asetetaan poistettava sieni, joka ottaa ekaListalla-muuttujan "paikan".
		Sieni poisto = ekaListalla;
		while(poisto != null && poisto.getSeuraava() != null)
		{
			paivamaara = poisto.getSeuraava().getKeraysPaiva(); //haetaan seuraava sieni ja haetaan keräyspäivä
			if ((paivamaara.get(Calendar.YEAR)== vuosi && paivamaara.get(Calendar.MONTH)==(kuukausi-1) && paivamaara.get(Calendar.DAY_OF_MONTH) ==paiva)
			   && laji.equals(poisto.getLaji())) {
			
				poisto.setSeuraava(poisto.getSeuraava().getSeuraava());
				poistettu = true; // sieni on poistettu, asetetaan arvoksi "true"
			}
			else
				poisto = poisto.getSeuraava(); // muutoin asetetaan seuraava alkio listassa
		}
		if (!poistettu) // Jos ei poistettu, annetaan ilmoitus ettei löytynyt
				return "Poistettavaa sientä ei löytynyt\n";
			else
				return "";
	}
  /**
   * <p>Sienten tulostusmetodi. Metodissa aloitetaan sienten tulostus listan ensimmäisestä alkiosta.
   * sSienet-muuttujaan kerätätään getSeuraava-metodin avulla sieniä ja joka kierroksella se saa lisää tulostettavaa tulostus-metodista, joka laitetaan kierroksen päätteeksi viittaamaan
   * seuraavaan sieneen. Sieni haetaan ja tämä toistuu, kunnes listalla ei enää ole sieniä.</p>
   * @return kaikki sienet String-muodossa
   *
   */

	public String tulostaSienet() {
		/** tulostettavaksi laitetaan listan ensimmäinen sieni */
		Sieni tulostus = ekaListalla;
		/* tyhjä String, johon kerätään tulostettavia sieniä */
		String sSienet = " ";
		while(tulostus != null) { // kun tulostettavaa on....
								  //Sienet-muttujaan lisätään tulostettavat sienet rivinvaihdon kanssa

			sSienet = tulostus + "\n" + sSienet;
			tulostus = tulostus.getSeuraava();
		}
		return sSienet;
	}

 }
