# README #

Vuonna 2012 tekemäni perusopintojen harjoitustyö. Ohjelmassa lisätään ja poistetaan sieniä listasta, tallennetaan niitä tiedostoon, luetaan tiedostosta ja tulostetaan tietoja näytölle. 
Harjoitustyö toimi siis harjoituksena olio-ohjelmoinnista. Laitettu BitBucketiin lähinnä nostalgiasyistä, tätä ei jatkokehitetä.

My 2012 basic studies assignment. The program adds and deletes mushrooms from a list, saves them to a file, reads them from a file and prints the data on screen. 
This assignment therefore functioned as an exercise in object-oriented programming. Added on BitBucket mainly for nostalgia reasons, this will not be further developed.