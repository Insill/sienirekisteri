/**
 * @(#)Sienirekisteri.java
 *
 * <p>Luokka, jossa hoidetaan tietorakenteen (Sieni) käsittely eri muodoissaan.
 * Erilaisia toimintoja ovat sienien lisäys ja poisto rekisteristä, tulostus näytölle tai tekstitiedostoon ja tallennus sarjallistettuna binääritiedostoon</p>.
 *
 * @author Insill
 * @version 1.00 2012/2/15
 */

import java.io.*;
import java.util.*;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.FileWriter;

public class Sienirekisteri {
	/** Muuttuja, joka kertoo, onko lista tallennettu vai ei */
	private static boolean bListaTallessa = true;
   	/** Sienilista-olio */
	private static Sienilista lista;
	/** Sieni-olio */
	private static Sieni sieni;

	/** Päämetodi, jossa on ohjelman päävalikkorakenne metodien tasolla. */
    public static void main (String [] args) {
    	 	/* Aloitusmetodi */
    	 	aloitus();
    	 	/* Käyttäjän valinta kokonaislukuna */
    	 	int iValinta = 0;
    	 	do {
    	 		iValinta = valikko();
    	 		switch(iValinta) {
					case 0: lopeta(); break;
    	 			case 1: lisaaSieni(); break;
    	 			case 2: poistaSieni(); break;
    	 			case 3: hae(); break;
    	 			case 4: tulosta(); break;
    	 			case 5: avaaSieniLista(); break;
    	 			case 6: tallennaSieniLista(); break;

    	 		}
    	 	} while(iValinta > 0);
    }
		/** Ohjelman lopetusmetodi.
    	 * Jos käyttäjä on tehnyt listaansa muutoksaia, huomautetaan niistä ja kysytään haluaako hän tallentaa.
    	 * Jos käyttäjä ei ole tehnyt muutoksia, tulostetaan pelkästään ilmoitus ohjelman loppumisesta
    	 *
    	 */
    public static void lopeta() {

		/** Boolean-muuttuja, joka ilmaisee, onko lista tallennettu vai ei */
        if(!bListaTallessa)
        {
            System.out.println("Listalla on muutoksia, haluatko tallentaa?");
            if(Kysy.totuusarvo()) // Jos K niin hypätään tallennaSieniLista-metodiin
                tallennaSieniLista();
                System.out.println("\n------------\n Heippa ja tervetuloa uudestaan! \n------------\n");
            return;
        }
            System.out.println("\n------------\n Heippa ja tervetuloa uudestaan! \n------------\n");
    }

		/**
    	 * Sienten hakumetodi, jossa käyttäjä voi hakea sieniä nimen tai myrykyllisyystason (1-3) mukaan
    	 * Sisältää Switch-case-rakenteen riippuen käyttäjän valinnasta.
    	 * Valinnasta riippuen kutsutaan joko Sienilista-luokan haeSienetLaji-tai haeSienetMyrkky-metodia.
    	 *
    	 */

    public static void hae() {
		/** Lajin nimi */
		String laji = "";
		/** myrkyllisyys, kokonaisluku */
		int myrkyllisyys = 0;
		/** valintamuuttuja valintarakennetta varten */
		int valinta2 = 0;
		System.out.print(" ------------ \nHaetaanko sientä nimen vai myrkyllisyyden perusteella? \n 1 - Nimi \n 2 - Myrkyllisyys \n 0 - Takaisin valikkoon \n ------------ \n");

		do {	// Valintarakenne sen mukaan, haluaako käyttäjä hakea sientä nimen vai myrkyllisyyden mukaan
			valinta2 = Kysy.kluku();
			switch(valinta2) {
				case 1: System.out.print("\nAnna sienen laji\n "); // Kysytään lajia
						laji = Kysy.mjono();
						if (laji.length() > 1);
							// haeSienetLaji-metodin kutsuminen
							System.out.println(lista.haeSienetLaji(laji));
						break;
				case 2: System.out.println("\n(1 - ei myrkyllinen) \n(2 - melko myrkyllinen) \n(3 - tappava)\n  ------------ \n");
						myrkyllisyys = Kysy.kluku();
						// Jos myrkyllisyys on väliltä 1-3, haetaan
						if (myrkyllisyys < 4);
							System.out.println(lista.haeSienetMyrkky(myrkyllisyys));
						break;
				case 0: return;
			}
		} while(valinta2 < 1 || valinta2 > 2 );
    }
	
	 /**
	 * Metodi, jossa käyttäjän sienilista tallennetaan sarjallistettuna oliona binääritiedostoon.
	 * Tiedosto luodaan FileInput-syöttövirran ja ObjectInputStreamin avulla.
	 * Käyttäjältä kysytään tiedoston nimeä ja jos kirjoitetaan 0, päästään takaisin päävalikkoon.
	 * Tiedoston pääte lisätään automaattisesti.
	 *
	 * @exception e Mahdollinen ongelma tiedoston käsittelyssä
	 */
    public static void tallennaSieniLista() {

    	/** Tiedostonimi merkkijonomuodossa, tullaan kysymään käyttäjältä */
        String sTiedosto = "";

        File fTiedosto = null;

        do {
            System.out.print(" ------------ \n Anna tiedoston nimi\n( Jos haluat takaisin valikkoon, kirjoita 0 )\n ------------ \n");
            sTiedosto = Kysy.mjono(); // tiedostonimen kysyminen
            if (sTiedosto.equals("0"))
            	return;
            	/** Muuttuja luotavalle tiedostolle */
            fTiedosto = new File (sTiedosto);
        }
        while(sTiedosto.length() < 1 | fTiedosto.exists());

        try { // tiedoston luonti fyysisesti
			FileOutputStream fosTiedosto = new FileOutputStream(fTiedosto + ".dat"); // Lisätään tiedostonimeen automaattisesti pääte .dat"
			ObjectOutputStream obsTiedosto = new ObjectOutputStream(fosTiedosto);
			obsTiedosto.writeObject(lista);
			obsTiedosto.flush();
			obsTiedosto.close();
			fosTiedosto.close();
			System.out.println("\n ------------ \nLista tallennettu tiedostoon " + sTiedosto + "\n ------------ \n");
			bListaTallessa = true; // boolean-muuttuja, joka tarkistaa, onko lista tallessa
        }
        catch(Exception e) { // Muulloin kerrotaan ongelmista
        System.out.println("\n ------------ \nOngelmia tiedoston käsittelyssä\n ------------ \n" + e);
        }
    }
	/**
	 * Sienen lisäysmetodi, jossa kysytään sienen tarvitsemia tietoja (laji, myrkyllisyys, määrä ja keräyspäivämäärä)
	 * Sienen laji tarkastetaan ja jos käyttäjältä tulee nimen eteen tai sen jälkeen välilyöntejä, ne poistetaan.
	 * <code>bListaTallessa<code>-muuttuja saa lisäyksen myötä arvon <i>false</i>, mikä toimii merkkinä listaan tehdyistä muutoksista.
	 * @exception e Mahdolliset virheet lisätessä sientä, ne tulostetaan jos tulee.
	 *
	 */

    public static void lisaaSieni() {
   	 	int iPaiva = 0; // alustus, päivä, kuukausi, vuosi ja kalenterimuuttujana paiva
   	 	int iKk = 0;
   	 	int iVuosi = 0;
   	 	Calendar paiva = null;
   	 	/** Tyhjä merkkijono merkitsemään lajia, tullaan kysymään käyttäjältä */
		String sLaji = "";
		do {
	    	System.out.println("\n ------------ \nAnna sienen laji\n(Laita 0 jos haluat takaisin valikkoon)\n ------------ \n"); // Kysytään käyttäjältä sienen lajia
		    sLaji = Kysy.mjono();
		    if (sLaji.equals("0")) // Jos lajiksi 0, valikkoon takaisin
		    	return;
		    sLaji = sLaji.trim(); // jos käyttäjältä tulee välilyöntejä nimen eteen tai nimen taakse, poistetaan ne
		} while(!Sieni.tarkastaLaji(sLaji));


		System.out.println("\n ------------ \nKuinka monta sientä on kerätty?\n ------------ \n");
		int iMaara = Kysy.kluku();
		System.out.println
("\n ------------ \nKuinka myrkyllinen sieni on? \n(1 - ei myrkyllinen) \n(2 - melko myrkyllinen) \n(3 - tappava)\n  ------------ \n");
		int iMyrkky = Kysy.kluku();

		System.out.println("\n ------------ \nAnna keräyksen päivä:\n ------------ \n");
		iPaiva = Kysy.kluku();
		System.out.print("\n ------------ \nAnna keräyksen kuukausi:\n ------------ \n");
		iKk = Kysy.kluku();
		System.out.print("\n ------------ \nAnna keräyksen vuosi:\n ------------ \n");
		iVuosi = Kysy.kluku();
		paiva = Calendar.getInstance(); // Tarkistetaan päivämäärä Calendarin avulla
		paiva.clear();
		paiva.set(iVuosi, iKk-1, iPaiva);

		Sieni sieni2 = null;
		try { // Luodaan sieni-olio
			/** Luodaan sieni, joka saa argumentteinaan ylempänä annetut tiedot */
			sieni2 = new Sieni(sLaji, iMaara, iMyrkky, paiva);
		} catch (Exception e) {
			System.out.println("\n ------------ \nVIRHE:\n ------------ \n" + e); // Virheilmoitus varuilta
		}

		lista.lisaaSieni(sieni2); // Listaan lisätään sieni
		bListaTallessa = false; // Listaan on lisätty sieni ja muutos on tehty, arvoksi laitetaan false
    }
 /** Sienen poistometodi.
  *
  * Käyttäjältä kysytään sienen lajia ja keräyspäivämäärää. Tämän jälkeen kutsutaan Sienilista-luokan poistaSieni-metodia, jossa poisto suoritetaan.
  * bLista-muuttuja saa tämänkin myötä false-arvon, sillä muutos listaan on tapahtunut.
  * @exception Mahdollinen virhe, joka on tullut sientä lisättäessä, esim. kirjoitusvirhe.
  *
  */

   public static void poistaSieni() {
	System.out.print("\n ------------ \nAnna sienen laji:\n( Laita 0 jos haluat takaisin valikkoon )\n ------------ \n");
	String sLaji = Kysy.mjono(); // Kysellään käyttäjältä sienen tietoja
	if (sLaji.equals("0"))
		return;
	System.out.print("\n ------------ \nAnna sienen keräyksen vuosi:\n -------------- \n");
	int vuosi =  Kysy.kluku();
	System.out.print("\n ------------ \nAnna sienen keräyksen päivä:\n ------------- \n");
	int paiva = Kysy.kluku();
	System.out.print("\n ------------ \nAnna sienen keräyksen kuukausi:\n ----------------- \n");
	int kuukausi = Kysy.kluku();

	String sStatus = lista.poistaSieni(sLaji, paiva, kuukausi, vuosi); // Mennään lista.poistaSieni-metodiin
	if(sStatus.length() > 0) // sStatus, muuttujaan kerätään tietoja virhetilanteista, jos se saa sisältöä, tulostetaan virheilmoitus, muulloin poisto onnistuu
		System.out.println("\n ------------ \nVIRHE:\n ------------ \n" + sStatus); // virheilmoituksen tulostus
	else
		System.out.println("\n ------------ \nPoisto onnistui!\n ------------ \n");
	bListaTallessa = false; // Listaan on tehty muutos ja sieni on poistettu, laitetaan bListaTallessa-muuttuja falseksi
   }

   /**
    * Sienen lajin tarkistusmetodi, jossa katsotaan onko sienen laji kirjoitettu lisättäessä sientä.
    * @return false jos sienen laji on sama
    * @param laji Sienen laji
    *
    */

   public static boolean tarkastaLaji(String laji) {	// Lajin tarkastus
   	if(laji !=null && laji.length() > 0) // jos sienen laji on täytetty ja nollaa isompi
   		return true; // tarkastus onnistuu
   	return false; // muulloin laji on sama, ei onnistu
   }

   /**
    * Aloitusmetodi, joka ajetaan ohjelman käynnistyksen yhteydessä.
    * Metodi luo oletusarvoisesti uuden tyhjän listan.
    *
    */
    public static void aloitus() {
    							// Aina aloitettaessa luodaan uusi sienilista
		lista = new Sienilista();
    }

	/**
	 * <p>Listan avausmetodi. Käyttäjältä kysytään tiedoston nimeä. Tiedoston kelpoisuuden määrittelyyn käytetään boolean-tyyppistä <code>bHyvaTiedosto</code>-muuttujaa,
	 * joka on oletusarvoisesti <b>tosi</b>. Jos tiedosto ei kuitenkaan ole olemassa <code>(!fTiedosto.exists())</code>, on piilotettu
	 * <code>(fTiedosto.isHidden())</code>, ei ole lukukelpoinen <code>(!fTiedosto.canRead())<code> tai on hakemisto, saa se arvon <b>epätosi</b>. Tällöin tiedoston avaus ei onnistu. </p>
	 * <p>Jos tiedosto onkin oikea, käytetään FileInputStreamia ja ObjectOutputStreamia tiedoston lukemiseen ja sarjallistuksen purkuun. Käyttäjälle ilmoitetaan, että tiedosto on avattu,
	 * muutoin ilmoitetaan poikkeuksista.</p>
	 *
	 * @exception e Mahdollinen ongelma tiedoston käsittelyssä
	 */
    public static void avaaSieniLista() {

        String sTiedosto = ""; // alustus, oletusarvoisesti tiedostonimi on tyhjä
        File fTiedosto = null; // oletusarvoisesti tiedosto on tylsä
        /** Boolean-muuttuja merkitsemään, onko tiedosto hyvä */
        boolean bHyvaTiedosto = true;
        do {
            System.out.print("\n ------------ \nAnna tiedoston nimi: \n( Laita 0 jos haluat takaisin valikkoon )\n ------------ \n");
            sTiedosto = Kysy.mjono();
            if (sTiedosto.equals("0"))
            	return;
            fTiedosto = new File (sTiedosto + ".dat"); // Tiedoston pääte lisätään automaattisesti
          	bHyvaTiedosto = true; // Boolean-muuttuja merkitsemään, onko tiedosto hyvä


           if(!fTiedosto.exists()) { // jos tiedosto ei ole olemassa
                bHyvaTiedosto = false; // Jos ei täytä ehtoja niin False
                System.out.println("Tiedostoa ei löydy, anna toinen nimi\n");
			}
           else if(fTiedosto.isHidden()) { // jos tiedosto on piilotettu              
                bHyvaTiedosto = false;
                System.out.println("Antamasi tiedosto on piilotettu, anna toinen nimi\n");
			}
           else if(!fTiedosto.canRead()) { // jos annetaan väärä tiedosto
                bHyvaTiedosto = false;
                System.out.println("Antamaasi tiedostoa ei voitu lukea, anna toinen nimi\n");
			}
     		else if (fTiedosto.isDirectory()) { // kun annetaan tiedoston sijasta hakemisto
     			bHyvaTiedosto = false;
     			System.out.println("Antamasi nimi on hakemisto, anna toinen nimi!\n");
     		}

    	} while (sTiedosto.length() < 1 || !bHyvaTiedosto);


		try { // tiedoston luonti fyysisesti try-catch-loopissa
			FileInputStream fosTiedosto = new FileInputStream(fTiedosto);
			ObjectInputStream obsTiedosto = new ObjectInputStream(fosTiedosto);
			lista = (Sienilista) obsTiedosto.readObject();
			obsTiedosto.close();
			fosTiedosto.close();

			System.out.print("\n ------------ \nTiedosto " + sTiedosto + " avattu\n ------------ \n");
			}
			catch(Exception e) { // Muulloin kerrotaan ongelmista
				System.out.println("\n ------------ \nOngelmia tiedoston käsittelyssä\n ------------ \n");
			}
    }

    /**
     *Valikkometodi, jossa tulostetaan päävalikko ja eri vaihtoehdot.
     *
     *
     * @return iValinta Käyttäjän valitsema toiminto joka viedään päämetodille.
     */

    public static int valikko() {
    	int iValinta = 0; // valinta oletusarvoisesti 0
    	do{
			System.out.println("\n------------\nSIENIREKISTERI 1.0\n-------------\n");
			System.out.println("1. Lisää sieni ");
			System.out.println("2. Poista sieni ");
			System.out.println("3. Hae sieniä ");
			System.out.println("4. Tulosta sienet ");
			System.out.println("5. Avaa sienilista tiedostosta ");
			System.out.println("6. Tallenna sienilista tiedostoon ");
			System.out.println("0. Lopeta \n");
			iValinta = Kysy.kluku(); // Kysytään valintaa lukuna
    	} while(iValinta < 0);
	 if(iValinta >= 7) // Jos valinta on 7 tai suurempi niin tulostetaan virhe
		System.out.println("\n-----------------\nVirheellinen valinta!\n--------------\n");
		return iValinta;
    }
	/**
	 * <p>Sienten tulostusmetodi, jossa sienilista voidaan käyttäjän valinnasta riippuen tulostaa joko näytölle tai tekstitiedostoon.
	 * Valinta käydään läpi switch-case-rakenteella. Jos käyttäjä haluaa tekstitiedoston, kysytään häneltä tiedoston nimeä ja käytetään PrintWriteria luomaan tekstitiedosto.
	 * Tulostuksen päätteeksi kerrotaan, millä nimellä tiedosto on tallennettu.</p>
	 * @exception e Ongelma tekstitiedoston käsittelyssä
	 * @return palataan päävalikkoon jos käyttäjä niin valitsee tai tulee virheellinen valinta.
	 */

    public static void tulosta() { // sienten tulostus
    
		int valinta3 = 0; // Kysytään käyttäjältä haluaako hän tiedot näytölle vai tekstitiedostoon
		System.out.println("\n ------------------ \nTulostetaanko lista näytölle vai tekstitiedostoon?\n 1 - Näytölle \n 2 - Tekstitiedostoon \n 0 - Takaisin valikkoon \n -------------- \n");
		do {
			valinta3 = Kysy.kluku(); // Switch-Case-rakenne riippuen käyttäjän valinnasta
			switch(valinta3) {

					case 1: System.out.println(lista.tulostaSienet()); break; // tulostus näytölle
					case 2: System.out.println("\n ------------------ \nAnna listalle haluamasi tiedostonimi:\n ------------------ \n");
							String sTekstiTiedosto = Kysy.mjono(); // Kysytään käyttäjältä tiedostonimeä
							try {
								PrintWriter fTekstiTiedosto = new PrintWriter(new FileWriter(sTekstiTiedosto + ".txt")); // Tiedostopääte lisätään automaattisesti
								fTekstiTiedosto.println(lista.tulostaSienet()); // Luodaan PrintWriter, jonka avulla kirjoitetaan tekstitiedostoon
								fTekstiTiedosto.flush();
								fTekstiTiedosto.close();
								System.out.println("\n ------------ \nLista tallennettu tekstitiedostoon " + sTekstiTiedosto + "\n ------------ \n");
							}
							catch(Exception e) {// Virheiden varalta poikkeusten käsittely
								
								System.out.println("\n ------------ \nOngelmia tekstitiedoston käsittelyssä\n ------------ \n" + e);
							}
							break;
					case 0: return;

					default: System.out.println("Virheellinen valinta! \n");
							return;					// Jos valitaan muuta niin tulostetaan virhe
				}
			} while(valinta3 < 1 || valinta3 > 2);

	}
}

