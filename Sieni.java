/**
 * @(#)Sienirekisteri.java
 *
 * <p>Alkioluokka, joka toteuttaa sienen. Sienen tietoja ovat laji, määrä, myrkyllisyys ja keräyspäivämäärä.
 *	Koska luokkaa hyödynnetään linkitetyssä listassa, on yhtenä ominaismuuttujana myös linkki seuraavaan sieneen.</p>
 *
 * <p>Luokassa toteutetaan Serializable-rajapinta, jotta tietoja voidaan käsitellä binäärimuotoisena.<p>
 * @author Insill
 * @version 1.00 2012/1/23
 */

import java.util.Calendar;
import java.util.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;

public class Sieni implements Serializable {

 /** Tiedot kerätystä sienestä */

  	/** Sienen laji */
 	private String sLaji;
 	/** Kerätty määrä sientä */
	private int iMaara;
	/** Sienen myrkyllisyys asteikolla 1-3 */
	private int iMyrkyllisyys;
	/** Sienen keräyspäivämäärä (päivä, kuukausi ja vuosi) **/
	private Calendar calKeraysPaiva;
	/** Sienen seuraaja tietorakenteessa */
	private Sieni seuraava;

 /* Luo keskeytysten käsittelyn <code>Sieni</code>-oliota varten */
    public Sieni() throws Exception {
	    throw new Exception("");
    }

/** Konstruktori, joka luo <code>Sieni</code>-olion
  *
  * @param laji Sienen laji
  *@param maara Sienten määrä
  *@param myrkyllisyys Sienen myrkyllisyys
  *@param pvm Sienen keräyksen päivämäärä
  *
  */
	public Sieni(String laji, int maara, int myrkyllisyys, Calendar pvm) throws Exception {

		String sVirhe = setLaji(laji); /* <code>Virhe</code>-stringin luonti */
		sVirhe += setMaara(maara); /* jos virheitä tulee jossain vaiheessa niin ne kootaan tähän Stringiin */
		sVirhe += setKeraysPaiva(pvm);
		sVirhe += setMyrkyllisyys(myrkyllisyys);

		if (sVirhe.length() > 0)	/* Jos virheitä tulee, hoidellaan poikkeus */
			throw new Exception(sVirhe);
	}

	/** Metodi, jossa tarkistetaan ja asetetaan kerättyjen sienten määrä
	 * @param maara Sienten määrä kokonaislukuna
	 * @return tyhjä string
	 *
	 */
    public String setMaara(int maara) {


    	if(maara < 0)
    		return "Määrä ei voi olla negatiivinen!";
		if(maara == 0)
			return "Määrä ei voi olla nolla!";
    	iMaara = maara; /* Palautetaan määrä, jos annetaan negatiivinen niin kerrotaan virheestä */
        return "";
    }

	/** Lajin tarkastusmetodi, jolle annetaan parametrinä sienen laji
	 *
	 * @param laji Sienen laji
	 * @return boolean-muotoinen tieto, onko sienen laji kelvollinen
	 **/
	public static boolean tarkastaLaji(String laji)	{

		if(laji != null & laji.length()> 0)
			return true;
		return false;
	}
	/** Get-metodi-jolla pyydetään sienen keräyspäivää
	 * @return Sienen keräyspäivä
	 *
	 */

	public Calendar getKeraysPaiva() {

		return calKeraysPaiva;
	}

    /** Get-metodi jolla pyydetään sienen myrkyllisyyttä
     * @return Sienen myrkyllisyys (kokonaisluku väliltä 1-3
     *
     *
     */
    public int getMyrkyllisyys() {

    	return iMyrkyllisyys;
    }

	/** Get-metodi jolla pyydetään sienen lajia
	 *@return sienen laji (String)
	 *
	 *
	 */

	public String getLaji() // get-metodi lajille {

		return sLaji;
	}

	/** Sienen keräyspäivämäärän asetusmetodi
	*
	* @param calKeraysPVM Sienen keräyspäivämäärä <code>Calendar</code>-muodossa
    * @return viesti tyhjästä päivämäärästä, jos <i>calKeraysPVM</i> on tyhjä
    * @return viesti jos asetettu sienen päivämäärä on tulevaisuudessa tai liian kaukana menneisyydessä
	* @return tyhjä merkkijono
	*/

	public String setKeraysPaiva(Calendar calKeraysPVM) {

		if (calKeraysPVM == null)
			return "Keräyspäivämäärä ei kelpaa!\n"; /* @return viesti tyhjästä päivämäärästä, jos tyhjä */

		Calendar nyt = Calendar.getInstance(); /* tallennetaan tähän nykyinen päivämäärä */
		Calendar liianvanha = Calendar.getInstance(); /* Vanhin mahdollinen päivämäärä joka voidaan asettaa on 1. tammikuuta 1900 */
		liianvanha.clear();
		liianvanha.set(1900,Calendar.JANUARY,1);

		if(calKeraysPVM.compareTo(liianvanha) < 0) // Verrataan annettua päivämäärää liianvanha-muuttujaan, jos vanhempi....
			return "Keräyspäivämäärä ei kelpaa, se on liian kaukana menneisyydessä!";
		if(nyt.compareTo(calKeraysPVM) < 0) // tarkistus, verrataan keräyspäivää nykyiseen
			return "Keräyspäivämäärä ei kelpaa, se on tulevaisuudessa!\n"; /* @return viesti päivämäärästä joka on tulevaisuudessa */

		calKeraysPaiva = calKeraysPVM;	// Asetetaan calKeraysPaivan arvoksi calKeraysPVM
		return ""; /*@return tyhjä merkkijono */

	}

		/** Set-metodi myrkyllisyydelle
		 * @param myrkyllisyys Sienen myrkyllisyys kokonaislukuna
		 * @return ilmoitus, jos käyttäjä on antanut lukuna suuremman myrkyllisyyden kuin 3
		 *
		 */
	public String setMyrkyllisyys (int myrkyllisyys) {

		if (myrkyllisyys > 0 && myrkyllisyys < 4) // jos myrkyllisyys on sallituissa rajoissa....
		{
			iMyrkyllisyys = myrkyllisyys; // .. asetetaan
			return "";
		}
			return "Myrkyllisyys on oltava väliltä 1-3! \n(1 - ei myrkyllinen) \n(2 - melko myrkyllinen) \n(3 - tappava)"; // Varoitetaan
	}
		/** Set-metodi sienen lajille
		 *
		 * @param laji Sienen laji merkkijonona
		 * @return Jos käyttäjä ei ole antanut lajia, ilmoitetaan
		 */
	public String setLaji(String laji) {

		if (laji != null && laji.length() > 0)
		{ // jos laji ei ole tyhjä ja pituus ei ole nolla
		sLaji = laji;
		return "";
		}
		return "Nimi ei saa olla tyhjä!\n"; // Muulloin ilmoitetaan

	}

		/** toString-metodi joka palauttaa sienen tiedot tekstinä, käytössä tulostettaessa näytölle ja tiedostoon
		 *
		 * @return sPalautus-merkkijono, johon on koottu sienen laji, määrä, myrkyllisyys ja keräyspäivämäärä
		 *
		 */
	public String toString() {

		String sPalautus = "";

	   sPalautus += sLaji + " \n"; /* Palautukseen lisätään olion attribuutit...... */
	   sPalautus += "Määrä: " + iMaara + ", ";
	   sPalautus += "Myrkyllisyys: " + iMyrkyllisyys + ", ";
	   SimpleDateFormat muoto = new SimpleDateFormat("dd.MM.yyyy \n");
	   sPalautus +="Keräysaika: " + muoto.format(calKeraysPaiva.getTime()) + "\n";

		return sPalautus;
	}

	/** Set-metodi, jolla asetetaan viittaus eli linkki seuraavaan sieneen tietorakenteessa
	 * @param viite Viittaus sieneen
	 */
	public void setSeuraava(Sieni viite) {
		seuraava = viite;
	}

    /** Get-metodi, jolla haetaan viite seuraavasta sienestä
	 * @return listan seuraava sieni
	 */
	public Sieni getSeuraava() {

		return seuraava;
	}
}
